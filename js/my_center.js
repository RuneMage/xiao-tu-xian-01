// 获取元素
//侧边栏渲染数据的元素获取
const xtx_personal_aside_arr = [
    {
        bigTitle: "我的账户",
        tips: [
            "个人中心",
            "消息通知",
            "个人信息",
            "地址管理",
            "我的积分",
            "我的足迹",
            "邀请有礼",
            "幸运抽奖",
            "交易管理",
        ],
    },
    {
        bigTitle: "我的订单",
        tips: ["礼品卡", "优惠券", "评价晒单", "售后服务"],
    },
    {
        bigTitle: "我的收藏",
        tips: ["收藏的商品", "收藏的专题", "关注的品牌"],
    },
    {
        bigTitle: "帮助中心",
        tips: ["帮助中心", "在线客服"],
    },
];
//兴趣分类界面数据获取
const hobby_classify_arr = [
    { src: 'https://yanxuan-item.nosdn.127.net/f81d1c188f7cad8f9f21780ddcf86ace.jpg', title: '生鲜' },
    { src: 'https://yanxuan-item.nosdn.127.net/d0176b163973961ed01818d04ee94a06.png', title: '美食' },
    { src: 'https://yanxuan-item.nosdn.127.net/fc266553a5b00280ab835e83a660ef14.jpg', title: '家电' },
    { src: 'https://yanxuan-item.nosdn.127.net/8896b897b3ec6639bbd1134d66b9715c.jpg', title: '餐厨' },
    { src: 'https://yjy-oss-files.oss-cn-zhangjiakou.aliyuncs.com/tuxian/popular_3.jpg', title: '女装' },
    { src: 'https://yanxuan-item.nosdn.127.net/17d32665d53e351aa499fb1e56796e58.jpg', title: '男装' },
    { src: 'https://yanxuan-item.nosdn.127.net/6d8de4504bdb9c1ad263e6b5b9a18ab2.jpg', title: '日用' },
    { src: 'https://yanxuan-item.nosdn.127.net/5c2abad66d86250b6c85c74ffd0ecc2d.png', title: '洗护' },
    { src: 'https://yanxuan-item.nosdn.127.net/1661541f80bc9776e64125b7d586c9e6.jpg', title: '孕婴' },
]
//选项卡面板获取
const personal_panels = document.querySelectorAll('.personal_panel>div')
//身材尺码输入框变量获取
const getEle = (className) => {
    return document.querySelector(className)
}
const xtx_personal_size_name = getEle('.xtx_personal_size_name')
const xtx_personal_size_gender = getEle('.xtx_personal_size_gender')
const xtx_personal_size_height = getEle('.xtx_personal_size_height')
const xtx_personal_size_weight = getEle('.xtx_personal_size_weight')
const xtx_personal_size_shoulder = getEle('.xtx_personal_size_shoulder')
const xtx_personal_size_chest = getEle('.xtx_personal_size_chest')
const xtx_personal_size_waistline = getEle('.xtx_personal_size_waistline')
const xtx_personal_size_hipline = getEle('.xtx_personal_size_hipline')
const xtx_personal_size_footsize = getEle('.xtx_personal_size_footsize')
const xtx_personal_size_footround = getEle('.xtx_personal_size_footround')

//获取兴趣渲染界面元素
const hobby_classify = getEle('.personal_hobby .hobby_classify')
//我的收藏的商品页面数据获取
const personal_collect_arr = [
    {
        src: 'https://yanxuan-item.nosdn.127.net/fcdcb840a0f5dd754bb8fd2157579012.jpg',
        title: '自煮火锅不排队 麦饭石不粘鸳鸯火锅',
        desc: '清汤鲜香 红汤劲爽',
        price: '¥159.00'
    },
    {
        src: 'https://yanxuan-item.nosdn.127.net/6d8de4504bdb9c1ad263e6b5b9a18ab2.jpg',
        title: '免浆黑鱼片，酸菜鱼350g',
        desc: '酸中带辣，开胃爽口酸菜鱼',
        price: '¥15.80'
    },
    {
        src: 'https://yanxuan-item.nosdn.127.net/17d32665d53e351aa499fb1e56796e58.jpg',
        title: '鲜活捕捞，爽口鱿鱼须250g*1包（约15g/根）',
        desc: '鲜甜海味，低脂低热量',
        price: '¥19.90'
    },
    {
        src: 'https://yanxuan-item.nosdn.127.net/39d1f00e557fbb44862dc5cbffe9ad09.png',
        title: '自煮火锅不排队麦饭石不粘鸳鸯火锅',
        desc: '清汤鲜香红汤劲爽',
        price: '¥159.00'
    },
    {
        src: 'https://yanxuan-item.nosdn.127.net/fcdcb840a0f5dd754bb8fd2157579012.jpg',
        title: '加热速食，手工包制早餐包',
        desc: '严选食材，汤汁浓郁，两种口味吃不够',
        price: '¥26.80'
    },
    {
        src: 'https://yanxuan-item.nosdn.127.net/0d0cd4fc226d0daf834f55586a057e7e.jpg',
        title: '舟山特色，即食野生小海鲜',
        desc: '每日现做，弹滑鲜香',
        price: '¥26.80'
    },
    {
        src: 'https://yanxuan-item.nosdn.127.net/16e0f76367c3184ef64431a5676e69c0.png',
        title: '可以喝的水果面膜韩国蜂蜜柚子茶560克',
        desc: '维C满满，营养丰富',
        price: '¥35.00'
    },
    {
        src: 'https://yanxuan-item.nosdn.127.net/d5b7af02c7a2cf3966385d6565a2501d.png',
        title: '深船型两带式可外穿软弹拖鞋',
        desc: '软弹如云朵般，开天窗版透气',
        price: '¥27.50'
    },
]








// 侧边栏数据渲染
function xtx_personal_aside_render(arr) {
    document.querySelector(".xtx_personal_aside").innerHTML = arr.map(item => `<dl>
                <dt>${item.bigTitle}</dt>
                <dd>
                    ${item.tips.map((item, index) => `
                    <a href="javascript:;">${item}</a>
                    ` ).join("")}
                </dd>
                </dl>
    `  ).join("");
}
xtx_personal_aside_render(xtx_personal_aside_arr)
//侧边栏点击事件绑定
const xtx_personal_article_panel = document.querySelectorAll('.xtx_personal_article_panel>div')
console.log(xtx_personal_article_panel);
//每一个主面板设置自定义下标属性
xtx_personal_article_panel.forEach((item, index) => item.dataset.index = index)
//给每一个侧边导航栏设置自定义下标属性
document.querySelectorAll('.xtx_personal_aside dd a').forEach((item, index) => item.dataset.index = index)
//事件委托绑定
document.querySelector('.xtx_personal_aside').addEventListener('click', function (e) {
    console.log(e.target);
    if (e.target.tagName === "A") {
        document.querySelector('.xtx_personal_aside dd a.active')?.classList.remove('active')
        e.target.classList.add('active')
        xtx_personal_article_panel.forEach(item => item.style.display = 'none')
        xtx_personal_article_panel[e.target.dataset.index].style.display = 'block'
    }
})



//tab栏切换（选项卡）
//添加自定义属性
document.querySelectorAll('.personal_feedtab li a').forEach((item, index) => {
    item.dataset.index = index
})

//事件委托绑定点击事件
document.querySelector('.personal_feedtab ul').addEventListener('click', function (e) {
    if (e.target.tagName === 'A') {
        e.preventDefault()
        document.querySelector('.personal_feedtab li.active')?.classList.remove('active')
        e.target.parentNode.classList.add('active')
        personal_panels.forEach(item => item.style.display = 'none')
        personal_panels[e.target.dataset.index].style.display = 'flex'
    }
})
//身材尺码表格数据获取和渲染
const personal_size_data = get_personal_size_data()
function get_personal_size_data() {
    const str = localStorage.getItem('personal_size')
    return str ? JSON.parse(str) : []
}
//封装存贮数据函数
function set_personal_size_data() {
    localStorage.setItem('personal_size', JSON.stringify(personal_size_data))
}
xtx_personal_size_render(personal_size_data)
function xtx_personal_size_render(arr) {
    document.querySelector('.personal_size .size_table').innerHTML = arr.map(item => `
            <tr>
                <td>性别:${item.gender}</td>
                <td>身高(cm):${item.height}</td>
                <td>体重(kg):${item.weight}</td>
            </tr>
            <tr>
                <td>肩宽(cm):${item.shoulder}</td>
                <td>胸围(cm):${item.chest}</td>
                <td>腰围(cm):${item.waistline}</td>
            </tr>
            <tr>
                <td>臀围(cm):${item.hipline}</td>
                <td>脚长(cm):${item.footsize}</td>
                <td>脚围(cm):${item.footround}</td>
            </tr>
    `).join('')
}
document.querySelector('.xtx_page_cover .set_info .save_btn').addEventListener('click', function () {
    if (xtx_personal_size_name.value.trim() === "" ||
        xtx_personal_size_gender.value.trim() === "" ||
        xtx_personal_size_height.value.trim() === "" ||
        xtx_personal_size_weight.value.trim() === "" ||
        xtx_personal_size_shoulder.value.trim() === "" ||
        xtx_personal_size_chest.value.trim() === "" ||
        xtx_personal_size_waistline.value.trim() === "" || xtx_personal_size_hipline.value.trim() === "" || xtx_personal_size_footsize.value.trim() === "" || xtx_personal_size_footround.value.trim() === "") return alert('输入内容不能为空！')
    const xtx_personal_size_obj = {
        gender: xtx_personal_size_gender.value,
        height: xtx_personal_size_height.value,
        weight: xtx_personal_size_weight.value,
        shoulder: xtx_personal_size_shoulder.value,
        chest: xtx_personal_size_chest.value,
        waistline: xtx_personal_size_waistline.value,
        hipline: xtx_personal_size_hipline.value,
        footsize: xtx_personal_size_footsize.value,
        footround: xtx_personal_size_footround.value,
    }
    personal_size_data.push(xtx_personal_size_obj)
    set_personal_size_data()
    xtx_personal_size_render(personal_size_data)
    document.querySelector('.xtx_page_cover').style.display = 'none'
})
//弹层页面显示
document.querySelector('.personal_size .set_newsize').addEventListener('click', function () {
    document.querySelector('.xtx_page_cover').style.display = 'block'
    //点击保存
    document.querySelector('.save_btn').addEventListener('click', function () {
        //渲染到页面
    })
    //点击取消
    document.querySelector('.cancel_btn').addEventListener('click', function () {
        document.querySelector('.xtx_page_cover').style.display = 'none'
    })
})



//兴趣分类界面渲染
function hobby_classify_render(arr) {
    document.querySelector('.personal_hobby .hobby_classify').innerHTML = arr.map((item) => `
    <li>
        <label for="${item.title}">
            <img src="${item.src}">
                <div class="hobby_choose">
                    <input type="checkbox" name="" id="${item.title}">
                    <span>${item.title}</span>
                </div>
        </label>
    </li>
`).join('')
}
hobby_classify_render(hobby_classify_arr)
const arr = document.querySelectorAll('.personal_hobby .hobby_classify li')
// 事件委托
document.querySelector('.personal_hobby .hobby_classify').addEventListener('click', function (e) {
    if (e.target.tagName === "INPUT") {
        e.target.parentNode.parentNode.classList.add('active')
        if (!e.target.checked) {
            e.target.parentNode.parentNode.classList.remove('active')
        }
    }
})

//我的收藏的商品页面数据渲染
function personal_collect_render(arr) {
    document.querySelector('.xtx_personal_collect .personal_collect_content').innerHTML = arr.map((item) => `
    <li>
        <a href="#">
            <img src="${item.src}" alt="">
            <p class="collect_title">${item.title}</p>
            <p class="collect_desc">${item.desc}</p>
            <div class="collect_price">${item.price}</div>
            <button class="collect_similar_btn">找相似</button>
        </a>
    </li>
`).join('')
}
personal_collect_render(personal_collect_arr)
//鼠标划上事件
document.querySelector('.personal_collect_content').addEventListener('mouseover', function (e) {
    if (e.target.tagName === "A") {
        document.querySelector('.personal_collect_content li.active')?.classList.remove('active')
        e.target.parentNode.classList.add('active')
    }
})
document.querySelector('.personal_collect_content').addEventListener('mouseout', function (e) {
    if (e.target.tagName === "A") {
        e.target.parentNode.classList.remove('active')
    }
})

