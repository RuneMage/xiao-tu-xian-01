// 分类导航栏 start
// 2.白色主导航栏部分

// 中间导航模块
let xtx_nav = ['首页', '生鲜', '美食', '餐厨', '电器', '居家', '洗护', '孕婴', '服装']
// 获取所有的导航栏的ul标签，map循环元素，修改其innerHTML。
// 循环里面的数据，使用模板字符串，生成相同长度的li数组，使用join拼接成一个大的字符串，并渲染到页面
Array.from(document.querySelectorAll('.xtx_head_nav .xtx_nav ul')).map(item => {
  return item.innerHTML = xtx_nav.map(item => `<li><a href="#">${item}</a></li>`).join('')
}).join('')

// 3.隐藏导航栏部分
// 右边导航模块
// 获取导航栏的ul标签，map循环元素，修改其innerHTML。
// 循环里面的数据，使用模板字符串，生成相同长度的li数组，使用join拼接成一个大的字符串，并渲染到页面
let xtx_nav_hide = ['品牌', '专题']
document.querySelector('.xtx_head_nav_hide .xtx_nav_hide ul').innerHTML = xtx_nav_hide.map(item => `<li><a href="#">${item}</a></li>`).join('')
// 下拉到固定位置显示:
// 给页面添加滚动事件, 当document.documentElement.scrollTop(页面卷去的头部)>最顶部导航栏距离页面顶部的距离时,隐藏导航栏显示
// window.addEventListener('scroll', function () {
//   if (document.documentElement.scrollTop > document.querySelector('.xtx_head').offsetTop) {
//     document.querySelector('.xtx_head_nav_hide').style.height = '90px'
//     document.querySelector('.xtx_head_nav_hide').style.opacity = '1'
//   }
// })
// // 给页面添加滚动事件, 当document.documentElement.scrollTop(页面卷去的头部)===0,隐藏导航栏隐藏
// window.addEventListener('scroll', function () {
//   if (document.documentElement.scrollTop === 0) {
//     document.querySelector('.xtx_head_nav_hide').style.opacity = '0'
//     document.querySelector('.xtx_head_nav_hide').style.height = '0'
//   }
// })


// 底部模块
// 底部的下面部分-- ul部分
let footer_slogan = [
  {
    iconName: 'icon-zuopin-01-01',
    title: '价格亲民'
  },
  {
    iconName: 'icon-dianzan',
    title: '物流快捷'
  },
  {
    iconName: 'icon-wuliupeisong',
    title: '品质新鲜'
  }
]
document.querySelector('.footer_slogan').innerHTML = footer_slogan.map(item => `
        <li>
            <i class="iconfont ${item.iconName}"></i>
            <span>${item.title}</span>
        </li>
    `).join('')
// 下面的版权部分
let footer_copyright = ['关于我们', '帮助中心', '售后服务', '配送与验收', '商务合作', '搜索推荐', '友情链接']
document.querySelector('.xtx_footer .footer_copyright p:first-child').innerHTML = footer_copyright.map(item => `<a href="#">${item}</a>`).join('')
